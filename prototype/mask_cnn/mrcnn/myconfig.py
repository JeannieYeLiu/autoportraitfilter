class myconfig(object):
    IS_DEBUG = False

    GET_FRAME = True

    IS_VIDEO = True
    IS_REALTIME = False
    assert IS_VIDEO | IS_REALTIME == True

    FRAME_SIZE = 10
    DEBUG_PIC_NAME = "Test"


    CLASS_NAMES = [
    'BG', 'person', 'bicycle', 'car', 'motorcycle', 'airplane',
    'bus', 'train', 'truck', 'boat', 'traffic light',
    'fire hydrant', 'stop sign', 'parking meter', 'bench', 'bird',
    'cat', 'dog', 'horse', 'sheep', 'cow', 'elephant', 'bear',
    'zebra', 'giraffe', 'backpack', 'umbrella', 'handbag', 'tie',
    'suitcase', 'frisbee', 'skis', 'snowboard', 'sports ball',
    'kite', 'baseball bat', 'baseball glove', 'skateboard',
    'surfboard', 'tennis racket', 'bottle', 'wine glass', 'cup',
    'fork', 'knife', 'spoon', 'bowl', 'banana', 'apple',
    'sandwich', 'orange', 'broccoli', 'carrot', 'hot dog', 'pizza',
    'donut', 'cake', 'chair', 'couch', 'potted plant', 'bed',
    'dining table', 'toilet', 'tv', 'laptop', 'mouse', 'remote',
    'keyboard', 'cell phone', 'microwave', 'oven', 'toaster',
    'sink', 'refrigerator', 'book', 'clock', 'vase', 'scissors',
    'teddy bear', 'hair drier', 'toothbrush'
]


    VIDEO_NAME = "teamwalking.mp4"
    OUTPUT_NAME = "people_after.mp4"


    # deepsort
    MAX_COSINE_DISTANCE = 0.3
    NN_BUDGET = None
    DEEPSORT_MODEL_DIR = 'model_data/mars-small128.pb'