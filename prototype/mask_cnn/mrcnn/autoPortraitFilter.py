from __future__ import division, print_function, absolute_import

import numpy as np
import argparse
import time
import cv2
import model
from visualize import display_instances, random_colors, apply_mask
import sys
import os
import skimage.io
from creat_model import model
import math

import os
from timeit import time
import warnings
import sys
import cv2
import numpy as np
from PIL import Image

from deep_sort import preprocessing
from deep_sort import nn_matching
from deep_sort.detection import Detection
from deep_sort.tracker import Tracker
from tools import generate_detections as gdet

from myconfig import myconfig


# specify object to be detected
# TODO make sure multiple ids work
def object_specify(detection_result, object_id):
    detection_mask = detection_result['class_ids'] == object_id
    rects = detection_result['rois'][detection_mask]
    scores = detection_result['scores'][detection_mask]
    masks = detection_result['masks'][:, :, detection_mask]
    return rects, scores, masks

def _yxyx_to_xywh(bbox_yxyx):
    bbox_xywh = bbox_yxyx.copy()
    bbox_xywh[:,0] = bbox_yxyx[:,1]
    bbox_xywh[:,1] = bbox_yxyx[:,0]
    bbox_xywh[:,2] = bbox_yxyx[:,3] - bbox_yxyx[:,1]
    bbox_xywh[:,3] = bbox_yxyx[:,2] - bbox_yxyx[:,0]
    return bbox_xywh


def initial_video_output(cap, myconfig):
    size = (
        int(cap.get(cv2.CAP_PROP_FRAME_WIDTH)),
        int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))
    )
    fps = cap.get(cv2.CAP_PROP_FPS)
    codec = cv2.VideoWriter_fourcc(*'DIVX')
    output_path = os.path.join('./video_after', myconfig.OUTPUT_NAME)
    output = cv2.VideoWriter(output_path, codec, fps, size)
    return output, output_path

## video setting


# progressing bar
#bar_length = 50
#total_frames = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))

def run(myconfig):
    if myconfig.IS_VIDEO:
        video_name = myconfig.VIDEO_NAME
        video_path = os.path.join('./video', video_name)
        cap = cv2.VideoCapture(video_path)
        output, output_path = initial_video_output(cap, myconfig)
    else:
        cap = cap = cv2.VideoCapture(0)


    ## Deepsort initialize
    encoder = gdet.create_box_encoder(myconfig.DEEPSORT_MODEL_DIR, batch_size=1)
    metric = nn_matching.NearestNeighborDistanceMetric("cosine", myconfig.MAX_COSINE_DISTANCE, myconfig.NN_BUDGET)
    tracker = Tracker(metric)

    count = 0
    while cap.isOpened():

        count += 1;

        if count == myconfig.FRAME_SIZE:
            break;

        ret, frame = cap.read()
        results = model.detect([frame], verbose=0)
        r = results[0]
        human_id = 1
        rects, scores, mask = object_specify(r,human_id)
        rects = _yxyx_to_xywh(rects)

        seqs = [i for i in range(rects.shape[0])]
        features = encoder(frame, rects)
        detections = [Detection(bbox, score, feature, seq) for seq, bbox, score, feature
                      in zip(seqs, rects, scores, features)]

        tracker.predict()
        tracker.update(detections)

        for track in tracker.tracks:
            if not track.is_confirmed() or track.time_since_update > 1:
                continue
            if track.seq >= len(seqs):
                continue
            bbox = track.to_tlbr()
            seq = track.seq

            cv2.rectangle(frame, (int(bbox[0]), int(bbox[1])), (int(bbox[2]), int(bbox[3])), (255, 255, 255), 2)
            cv2.putText(frame, str(track.track_id), (int(bbox[0]), int(bbox[1])), 0, 5e-3 * 200, (0, 255, 0), 2)
            frame = apply_mask(frame, mask[:, :, seq], track.track_id, alpha=1)

        if myconfig.GET_FRAME:
            framename = myconfig.DEBUG_PIC_NAME + str(count)+".jpg"
            framename = os.path.join('./frames', framename)
            if os._exists(framename):
                os.unlink(framename)
            cv2.imwrite(framename, frame)

        if myconfig.IS_VIDEO:
            if os._exists(output_path):
                os.unlink(output_path)
            output.write(frame)
            print('........frame:', count)
            #progress = math.ceil(bar_length * count / total_frames)
            #print("\r", "[" + "=" * progress + " " * (bar_length - progress) + "] " + "{0:.2f}".format(
             #   100 * count / total_frames) + '%', end="")

        if myconfig.IS_REALTIME:
            cv2.imshow('realtime',frame)
            if cv2.waitKey(1) & 0xFF == ord('q'):
                break

    if myconfig.IS_REALTIME:
        cap.release()
        cv2.destroyAllWindows()

run(myconfig)

