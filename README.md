# AutoPortraitFilter

Auto Portrait Filter in pictures and videos.
Due to a large number of tourists, especially during the holidays, it is difficult to take satisfactory pictures in travelling places filled with tourists.
Some people may choose to edit images and videos 

## Set up the environment

run the following command in terminal:
```
cd prototype/mask_cnn
pip3 install -r requirements.txt
```

run object tracking program:
```
cd prototype/mask_cnn/mrcnn
python3 autoportraitfilter.py

```