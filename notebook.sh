#!/bin/bash
ip=$(ifconfig en0 | grep inet | awk '$1=="inet" {print $2}')
xhost + $ip

relativeDir="UTS/summer_studio/autoportraitfilter"
docker run --rm -it \
	-e DISPLAY=$ip:0 \
	-p 8888:8888 \
	--volume="/tmp/.X11-unix:/tmp/.X11-unix:rw" \
	--volume="$HOME/$relativeDir:/root/prototype" \
	--privileged \
	1283740311/first:first  \
	jupyter lab --ip 0.0.0.0 --port 8888 --allow-root
