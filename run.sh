#!/bin/bash
ip=$(ifconfig en0 | grep inet | awk '$1=="inet" {print $2}')
xhost + $ip


relativeDir="/Users/wukailei/Documents/study/Australia/2020_summer/gitcode/working"

docker run --rm -it \
	-e DISPLAY=$ip:0 \
	-p 8888:8888 \
        --gpus all \
	--volume="/tmp/.X11-unix:/tmp/.X11-unix:rw" \
	--volume="$HOME/$relativeDir:/root/prototype" \
	--privileged \
	benthepleb/summerstudio2020:latest \
	bash
