#!/bin/bash

# Install NVIDIA driver

# Use this script to install CUDA

# https://developer.nvidia.com/cuda-downloads?target_os=Linux&target_arch=x86_64&target_distro=Ubuntu&target_version=1804
# 1. 安装
echo "Checking for CUDA and installing."
# Check for CUDA and try to install.

# 解决add-apt-repository not found 问题
sudo apt-get update
sudo apt-get install software-properties-common
sudo apt-get install python-software-properties

# 使用网络安装
wget https://developer.download.nvidia.com/compute/cuda/repos/ubuntu1804/x86_64/cuda-ubuntu1804.pin
sudo mv cuda-ubuntu1804.pin /etc/apt/preferences.d/cuda-repository-pin-600
sudo apt-key adv --fetch-keys https://developer.download.nvidia.com/compute/cuda/repos/ubuntu1804/x86_64/7fa2af80.pub
sudo add-apt-repository "deb http://developer.download.nvidia.com/compute/cuda/repos/ubuntu1804/x86_64/ /"
# update the repository you just add 
sudo apt-get update
sudo apt-get -y install cuda
# CUDA安装成功！

# 判断是否安装成功： 
nvidia smi

# 2. 安装 docker engine

echo "Install Docker engine"
# 如果装了docker，卸载旧版本docker
# sudo apt-get remove docker docker-engine docker.io
sudo apt install docker.io

# Method 2: 

sudo apt-get -y install 
# install packages to allow apt to use a repository on HTTPS
sudo apt-get install apt-transport-https ca-certificates curl software-properties-common

# add docker's official GPG key
# curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add –
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add - 

# 3.  安装 docker depository

echo "Install Docker depository"

#The command “$(lsb_release –cs)” scans and returns the codename of your Ubuntu installation 
#– in this case, Bionic. 
# Also, the final word of the command – stable– is the type of Docker release.

sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu  $(lsb_release -cs)  stable"

# update the repository you just add 
sudo apt-get -y update 

# install docker
sudo apt-get -y install docker-ce

# check docker service
docker --version


# 4. 安装nvidia-docker
# https://github.com/NVIDIA/nvidia-docker
# Add the package repositories

echo "install nvidia-docker"

distribution=$(. /etc/os-release;echo $ID$VERSION_ID)
curl -s -L https://nvidia.github.io/nvidia-docker/gpgkey | sudo apt-key add -
curl -s -L https://nvidia.github.io/nvidia-docker/$distribution/nvidia-docker.list | sudo tee /etc/apt/sources.list.d/nvidia-docker.list

sudo apt-get update && sudo apt-get install -y nvidia-container-toolkit
sudo systemctl restart docker

# 6. Verify GPU is visible from docker container

# verify
# sudo docker run --gpus all nvidia/cuda:9.0-base nvidia-smi

# #### Test nvidia-smi with the latest official CUDA image
# docker run --gpus all nvidia/cuda:9.0-base nvidia-smi

# # Start a GPU enabled container on two GPUs
# docker run --gpus 2 nvidia/cuda:9.0-base nvidia-smi

# # Starting a GPU enabled container on specific GPUs
# docker run --gpus '"device=1,2"' nvidia/cuda:9.0-base nvidia-smi
# docker run --gpus '"device=UUID-ABCDEF,1"' nvidia/cuda:9.0-base nvidia-smi

# # Specifying a capability (graphics, compute, ...) for my container
# # Note this is rarely if ever used this way
# docker run --gpus all,capabilities=utility nvidia/cuda:9.0-base nvidia-smi


# 7. Install Python3
echo "Install python3"
apt-get -y install python3-dev

# 8. Install Pip3

echo "Install pip3"
apt-get -y install python-pip
apt-get -y install python3-pip

# 9. Install iphython notebook
echo "Install Python notebook"
apt-get -y install ipython ipython-notebook

# 10. Install anaconda (conda装好之后，jupyter notebook 也可以用了)
echo "Install anaconda "
mkdir summer_studio
cd summer_studio
wget -P ./ "https://repo.anaconda.com/archive/Anaconda3-2019.10-Linux-x86_64.sh"

# Ensure the integrity of the installer with cryptographic hash verification through SHA-256 checksum:
sha256sum Anaconda3-2019.10-Linux-x86_64.sh

# run the downloaded script (连续敲3次 enter，如果yes/no 的，输入yes)
bash Anaconda3-2019.10-Linux-x86_64.sh
source ~/.bashrc

# 安装完成后，需要关掉重启 (输入exit命令即可)
# exit

# # 检测 Anaconda 是否安装成功.
# conda --version
# conda update anaconda-navigator
# conda update navigator-updater  
















